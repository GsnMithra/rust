use axum::{
    routing::{get, post},
    extract::{Path, Extension},
    http::StatusCode,
    Json, Router,
};
use tokio::net::TcpListener;

async fn root() -> &'static str {
    "Hello, Axum"
}

#[tokio::main]
async fn main() {
    let app = Router::new()
        .route("/", get(root));

    let listener = TcpListener::bind("0.0.0.0:2448")
        .await
        .unwrap();

    axum::serve(listener, app)
        .await
        .unwrap()
}
