// Generic lifetime when working with references

fn longest<'a>(a: &'a str, b: &'a str) -> &'a str {
    if a.len() >= b.len() {
        a
    } else {
        b
    }
}

fn main() {
    let a = String::from("Gsn");
    let b = String::from("Mithra");

    let result = longest(a.as_str(), b.as_str());

    println!("{}", result);
}
