struct Node {
    val: i32,
    next: Option<Box<Node>>,
}

impl Node {
    fn new(val: i32) -> Self {
        Node { val, next: None }
    }
}

fn main() {
    // creating three nodes
    let mut a = Node::new(1);
    let b = Node::new(2);
    let mut c = Node::new(3);

    // a next is b
    a.next = Some(Box::new(b));

    // c.next is also b
    // which causes error
    c.next = Some(Box::new(b));

    // since value already moved to a.next

    println!("{}", a.next.unwrap().val);
}
