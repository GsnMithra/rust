use std::collections::HashMap;
use std::collections::HashSet;
use std::collections::VecDeque;

fn main() {
    let edges = vec![vec![1, 2], vec![2, 3], vec![3, 4], vec![3, 2]];
    let mut adj: HashMap<i32, HashSet<i32>> = HashMap::new();

    for edge in edges.iter() {
        adj.entry(edge[0])
            .or_insert(HashSet::new())
            .insert(edge[1].clone());
        adj.entry(edge[1])
            .or_insert(HashSet::new())
            .insert(edge[0].clone());
    }

    println!("G: {:?}", adj);

    let mut q: VecDeque<i32> = VecDeque::new();
    let mut visited: HashSet<i32> = HashSet::new();
    let mut path: Vec<i32> = Vec::new();

    let start_node: i32 = 3;
    q.push_back(start_node);
    visited.insert(start_node);

    loop {
        let node = match q.pop_front() {
            Some(n) => n,
            None => break,
        };

        path.push(node);

        match adj.get(&node) {
            Some(neighs) => {
                for neigh in neighs {
                    if !visited.contains(&neigh) {
                        visited.insert(neigh.clone());
                        q.push_back(neigh.clone());
                    }
                }
            }
            None => continue,
        }
    }

    println!("{path:?}");
}
