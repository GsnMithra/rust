fn main() {
    let mut number = 10;

    loop {
        if number == 0 {
            break;
        }

        println!("Hello, world! {number}");
        number -= 1;
    }

    number = 10;
    while number != 0 {
        println!("Hello {number}");
        number -= 1;
    }

    for i in 1..=3 {
        println!("currently {i}");
    }
}
