fn main() {
    let value = get_some_value();

    let unwrapped = match value {
        Some(n) => n,
        None => 0,
    };

    println!("{}", unwrapped);
    println!("{}", value.unwrap());
}

fn get_some_value() -> Option<i32> {
    Some(10)
}
