use std::cmp;
use std::collections::VecDeque;
use std::time::{Duration, Instant};

fn valid_cell(grid: &Vec<Vec<i32>>, i: usize, j: usize) -> bool {
    cmp::min(i as i32, j as i32) >= 0 && i < grid.len() && j < grid[0].len() && grid[i][j] == 0
}

fn bfs_util(grid: Vec<Vec<i32>>, start: (usize, usize), end: (usize, usize)) {
    let (rows, cols) = (grid.len(), grid[0].len());

    let mut queue: VecDeque<(usize, usize, String)> = VecDeque::new();
    let mut visited: Vec<Vec<bool>> = vec![vec![false; cols]; rows];

    queue.push_back((start.0, start.1, String::from("")));
    visited[start.0][start.1] = true;

    let dx: Vec<i32> = vec![0, 0, -1, 1];
    let dy: Vec<i32> = vec![-1, 1, 0, 0];

    loop {
        let cell: (usize, usize, String) = match queue.pop_front() {
            Some(t) => t,
            None => break,
        };

        if (cell.0, cell.1) == end {
            println!("{}", cell.2);
            break;
        }

        for i in 0..=3 {
            let (nx, ny) = (
                ((cell.0 as i32) + dx[i]) as usize,
                ((cell.1 as i32) + dy[i]) as usize,
            );
            if valid_cell(&grid.clone(), nx, ny) && !visited[nx][ny] {
                let dir = match (dx[i], dy[i]) {
                    (0, -1) => "L".to_string(),
                    (0, 1) => "R".to_string(),
                    (-1, 0) => "U".to_string(),
                    (1, 0) => "D".to_string(),
                    _ => break,
                };

                queue.push_back((nx, ny, format!("{}{}", cell.2, dir)));
                visited[nx][ny] = true;
            }
        }
    }
}

fn main() {
    let grid = vec![
        vec![0, 0, 0, 0, 0, 0],
        vec![0, 1, 1, 0, 1, 1],
        vec![0, 1, 0, 0, 0, 0],
        vec![0, 1, 0, 1, 1, 1],
        vec![1, 1, 0, 1, 0, 0],
        vec![0, 0, 0, 0, 0, 1],
    ];

    let (start, end): ((usize, usize), (usize, usize)) = ((0, 0), (4, 4));

    let start_time = Instant::now();
    bfs_util(grid, start, end);
    let end_time = Instant::now();

    let elapsed_time: Duration = end_time - start_time;
    println!("Elapsed time: {} sec", elapsed_time.as_secs_f64());
}
