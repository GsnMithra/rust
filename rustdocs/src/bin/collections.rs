use std::collections::*;

enum HetrogeneousDType {
    Int(i32),
    Float(f32),
    Text(String),
}

fn main() {
    // Vectors

    let mut vector: Vec<i32> = vec![1, 2, 3, 4, 5];

    let value = vector.get(100);
    match value {
        Some(v) => println!("the unpacked value is: {}", v),
        None => println!("there was no such value!"),
    }

    // error because mutating when holding an immutable reference
    // let value = &vector[3];
    // vector.push(6);
    // println!("the value is {}", value);

    // mutable iteration
    for v in &mut vector {
        *v += 1;
    }

    // heterogeneous collections
    let _hetro_vector = vec![
        HetrogeneousDType::Int(10),
        HetrogeneousDType::Float(26.512),
        HetrogeneousDType::Text(String::from("Mithra")),
    ];

    // Strings

    let mut s = String::from("Gsn");
    s.push_str(" Mithra");

    let first = String::from("Gsn");
    let second = String::from("Mithra");

    // a, b variables then c = a + &b and not c = &a + b or anything else, becuase (+) uses add
    // function, which is defined as: fn add(self, s: &str) -> String
    // also, we will lose the value in `first` is no longer valid
    let full_name = first + &second;

    println!("{}", full_name);

    let string: String = String::from("Gsn");
    let another_string: String = String::from("Mithra");
    let slice: &str = "Mithra";

    // clone to use it in the next line.
    let _string_with_string = string.clone() + &another_string;
    let _string_with_slice = string + slice;

    let s1 = String::from("tic");
    let s2 = String::from("tac");
    let s3 = String::from("toe");

    let final_string = s1.clone() + "-" + &s2 + "-" + &s3;
    println!("the resultant string {}", final_string);

    // or simply
    let final_string = format!("{} {} {}", s1, s2, s3);
    let string_slice = &final_string[4..7].to_string();
    println!("{}", string_slice);

    // HashMap

    let mut map: HashMap<String, i32> = HashMap::new();
    map.insert("Gsn".to_string(), 19);
    map.insert("Mithra".to_string(), 20);

    let _age = map.get(&"Mithra".to_string()).copied().unwrap_or(-1);

    // iteration
    for (key, value) in &mut map {
        if *key == String::from("Gsn") {
            *value = 20;
        }
        println!("{}, {}", key, value);
    }

    let key = "John".to_string();
    let value = 20;
    map.insert(key, value);

    // error
    // println!("{}, {}", key, value);

    // word counting
    let sentence = "a quick brown fox jumps again over a quick brown fox";
    for word in sentence.split(" ") {
        // or_insert() returns a mutable reference to the value, so dereference it and mutate it.
        // let value = map.entry(word.to_string()).or_insert(0);
        // *value += 1;
        *map.entry(word.to_string()).or_insert(0) += 1;
    }
}
