// std::cmp::PartialOrd to apply the comparision of &a[i] > largest, otherwise would fail.
// wouldn't the code be slower?
//      no, rust compiler generates seperate code for all different versions of types
fn find_largest<T: std::cmp::PartialOrd>(a: &Vec<T>) -> &T {
    let mut largest = &a[0];
    let size = a.len();

    for i in 1..size {
        if &a[i] > largest {
            largest = &a[i];
        }
    }

    largest
}

#[allow(dead_code)]
struct Point<T> {
    x: T,
    y: T,
}

#[allow(dead_code)]
struct Coords<T, R> {
    x: T,
    y: R,
}

#[allow(dead_code)]
impl Coords<i32, f32> {
    fn rounded(self: &mut Self) -> Self {
        Coords {
            x: self.x,
            y: self.y.round(),
        }
    }
}

#[allow(dead_code)]
impl<K, V> Coords<K, V> {
    fn mixup<T>(self, other: Point<T>) -> Coords<K, T> {
        Coords {
            x: self.x,
            y: other.y,
        }
    }
}

#[allow(dead_code)]
impl<T> Point<T> {
    fn new(x: T, y: T) -> Self {
        Point { x, y }
    }

    fn x(self: &Self) -> &T {
        &self.x
    }

    fn y(self: &Self) -> &T {
        &self.y
    }
}

#[allow(dead_code)]
impl Point<f32> {
    fn distance_from_origin(self: &Self) -> f32 {
        ((self.x).powi(2) + (self.y).powi(2)).sqrt()
    }
}

fn main() {
    let a = vec![1, 2, 3, 4, 5];
    let _result = find_largest(&a);
    let _p = Point { x: 1, y: 3 };
    let _f = Point { x: 4.5, y: 6.8 };
    let mut c = Coords { x: 3, y: 5.3 };
    let _c = c.rounded();
}
