#[allow(unused, dead_code)]
#[derive(Debug)]
struct Student {
    name: String,
    age: u8,
    roll_number: i32,
}

// tuple structs
#[derive(Debug)]
struct Pair(i32, i32);

#[derive(Debug)]
struct Point(i32, i32);

// unit structs, will be useful with traits
// struct User;

fn create_user(name: String, age: u8, roll_number: i32) -> Student {
    Student {
        name,
        age,
        roll_number,
    }
}

fn main() {
    let first = create_user("Mithra".to_string(), 19, 2453);
    println!("{:?}", first);

    let second = Student {
        name: "Gsn Mithra".to_string(),
        ..first
    };

    let pair = Pair(10, 20);
    let point = Point(10, 20);

    // let result = pair == point; // error

    println!("{:?}, {:?}", pair, point);
    println!("{:?}, {:?}", first, second);

    let Pair(a, b) = pair;
    println!("{}, {}", a, b);
}
