// enum IPAddr {
//     V4,
//     V6,
// }

#[allow(dead_code, unused)]
#[derive(Debug)]
enum IPAddrVariant {
    V4(u8, u8, u8, u8),
    V6(String),
}

fn main() {
    let home = IPAddrVariant::V4(127, 0, 0, 1);
    let loopback = IPAddrVariant::V6("::1".to_string());

    dbg!(&home);
    dbg!(&loopback);
}
