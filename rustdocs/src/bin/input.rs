use std::io;
#[allow(dead_code)]
#[derive(Debug)]
struct Student {
    name: String,
    age: u8,
    roll_number: i32,
}

#[derive(Debug)]
struct User(String, u8);

fn get_new_user(name: String, age: u8, roll_number: i32) -> Student {
    Student {
        name,
        age,
        roll_number,
    }
}

fn main() {
    let mut name = String::new();

    // taking input
    io::stdin()
        .read_line(&mut name)
        .expect("name must be entered");

    let mut age = String::new();
    io::stdin()
        .read_line(&mut age)
        .expect("age must be entered");

    let mut roll_number = String::new();
    io::stdin()
        .read_line(&mut roll_number)
        .expect("roll_number must be entered");

    let mut stu = get_new_user(
        name,
        // must be trimmed to parse, because of the \n char
        age.trim().parse::<u8>().expect("age must be u8"),
        roll_number
            .trim()
            .parse::<i32>()
            .expect("roll_number must be i32"),
    );

    stu.name = "Gsn Mithra".to_string();

    let another_stu = Student {
        name: "Nitish".to_string(),
        ..stu
    };

    println!("{:?}", stu);
    println!("{:?}", another_stu);
}
