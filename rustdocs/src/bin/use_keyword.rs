#![allow(dead_code, unused)]

pub mod student_utils {
    pub struct Student {
        name: String,
        age: u8,
    }

    impl Student {
        pub fn new(name: String, age: u8) -> Self {
            Student { name, age }
        }

        pub fn change_name(self: &mut Self, new_name: String) {
            self.name = new_name;
        }
    }

    pub fn get_details_tuple(student: Student) -> (String, u8) {
        (student.name, student.age)
    }
}

// use keyword to bring {get_details_tuple, Student} into scope
// use crate::student_utils::{get_details_tuple, Student};
use std::collections::{HashMap, HashSet};
use student_utils::{get_details_tuple, Student};

// nested paths
// use std::cmp::Ordering;
// use std::io;
use std::{cmp::Ordering, io};

// glob operator to import everything inside
use std::collections::*;

fn main() {
    let mut student = Student::new("Gsn Mithra".to_string(), 20);
    student.change_name("Mithra".to_string());
}
