// using #[path = "path to module"] to access modules from /bin
// dont need to use this when accessing from main.rs or lib.rs

// #[path = "../randomizer.rs"]
// mod randomizer;
// use randomizer::{get, Randomize};

// fn main() {
//     println!("{}", get());
//     println!("{}", Randomize::get(20))
// }

// install cargo-modules
// run `cargo-modules structure --bin mod_testing`, to look at the module structure

/*
three ways to define modules
1) inline modules
    mod random {
        pub fn get_random() -> u32 {
            10
        }
    }

2) external modules
    create a file random.rs and define the module inside it
    to use it somewhere else, use `mod random;` and `use random::get_random;`
    also add `use random::get_random;` in the file where you want to use get_random

    random.rs

    ```
    mod random;
    use random::get_random;
    ```

3) nested modules
    create a folder random and create a file mod.rs inside it, define the module inside it
    to use it somewhere else, use `mod random;` and `use random::get_random;`
    then define more modules inside the random folder and use them in the same way

    Eg: random/mod.rs

        ```
        mod random;
        use random::get_random;
        ```

        random/random.rs

        ```
        fn get_random() -> u32 {
            10
        }
        ```

    another way to define nested modules is to name the file the same as the folder name
    and define the module inside it, submodules can be defined inside the folder.

*/

#[path = "../shopping_cart/mod.rs"]
mod shopping_cart;
use std::iter::zip;

use shopping_cart::{model::Item, ShoppingCart};

fn main() {
    let mut cart = ShoppingCart::new();

    let names: Vec<String> = vec![
        "Nutella".to_string(),
        "Yogurt".to_string(),
        "Ferrero Rocher".to_string(),
    ];
    let prices: Vec<u32> = vec![299, 159, 3999];

    for (name, price) in zip(names, prices) {
        cart.add_item(Item::new(name, Some(price)));
    }

    println!("{:#?}", cart.get_cart());

    cart.delete_item(1);

    println!("{:#?}", cart.get_cart());
}
