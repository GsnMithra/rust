#[derive(Debug)]
struct Rectange {
    height: u32,
    width: u32,
}

impl Rectange {
    // Self refers to Rectangle struct
    fn new(height: u32, width: u32) -> Self {
        Rectange { height, width }
    }

    // `self: &Self` is same as just saying `&self`
    fn area(self: &Self) -> u32 {
        self.height * self.width
    }

    fn can_hold(self: &Self, other: &Rectange) -> bool {
        other.height <= self.height && other.width <= self.width
    }
}

fn main() {
    let first = Rectange::new(10, 20);
    let second = Rectange::new(5, 10);

    println!("{}", second.can_hold(&first));
}
