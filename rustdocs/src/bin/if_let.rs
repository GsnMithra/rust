use rand::Rng;

fn get_me_coin() -> Option<i32> {
    if rand::thread_rng().gen_range(0..=1) == 0 {
        None
    } else {
        Some(1)
    }
}

fn main() {
    let coin = get_me_coin();

    // match coin {
    //     Some(c) => println!("we got a coin, {}", c),
    //     None => {}
    // }

    // used for single arm matching
    if let Some(c) = get_me_coin() {
        println!("we got a coin, {}", c);
    }
}
