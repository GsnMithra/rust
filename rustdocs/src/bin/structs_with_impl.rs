use std::fmt;

struct Rectangle {
    height: u32,
    width: u32,
}

impl Rectangle {
    fn new(height: u32, width: u32) -> Self {
        Rectangle { height, width }
    }

    fn area(&self) -> u32 {
        self.height * self.width
    }
}

// custom toString() method equivalent from Java in Rust
impl fmt::Debug for Rectangle {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Rectangle ({}, {})", self.height, self.width)
    }
}

fn main() {
    let rect = Rectangle {
        height: 30,
        width: 50,
    };

    let another_rect = Rectangle::new(10, 20);

    println!("{}", rect.area());
    println!("{}", another_rect.area());
}
