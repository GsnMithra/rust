use std::io::Read;

// cannot propagate Result <T, E> to Option <K> or vice-versa
// but can use .ok () for converting Result <T, E> to Option <T>
// and can use .ok_or (<E>) for converting Option <T> into Result <T, E>

struct CustomError;
impl From<std::io::Error> for CustomError {
    fn from(_: std::io::Error) -> Self {
        Self
    }
}

fn main() {
    let file_name = "hello.txt";
    let username = read_username(file_name);
    println!("{}", username.unwrap_or("johndoe".to_string()));

    let file_name = "username.txt";
    let username = read_username_propagation(file_name);
    println!("{}", username.unwrap_or("johndoe".to_string()));

    let file_name = "username.txt";
    let username = read_username_easy(file_name);
    println!("{}", username.unwrap_or("johndoe".to_string()));

    // option propagation
    let string = String::from("Hello, world!");
    match string_at_ix_to_upper(&string, 100) {
        Some(s) => println!("{}", s),
        None => println!("nothing to make uppercase!"),
    }
}

fn string_at_ix_to_upper(string: &str, ix: usize) -> Option<char> {
    Some(string.chars().nth(ix)?.to_ascii_uppercase())
}

fn read_username_easy(file_name: &str) -> Result<String, CustomError> {
    Ok(std::fs::read_to_string(file_name)?)
}

// custom error propagation
fn read_username_propagation(file_name: &str) -> Result<String, CustomError> {
    // let mut file = std::fs::File::open(file_name)?;
    // let mut contents = String::new();
    // file.read_to_string(&mut contents)?;
    // Ok(contents)

    let mut contents = String::new();
    std::fs::File::open(file_name)?.read_to_string(&mut contents)?;
    Ok(contents)
}

fn read_username(file_name: &str) -> Result<String, std::io::Error> {
    let file_result = std::fs::File::open(file_name);

    let mut file = match file_result {
        Ok(f) => f,
        Err(e) => return Err(e),
    };

    let mut contents = String::new();

    match file.read_to_string(&mut contents) {
        Ok(_) => Ok(contents),
        Err(e) => Err(e),
    }
}
