use std::{fs::File, io::Read};

fn main() {
    let file_name = "sample.txt";
    match File::open(file_name) {
        Ok(mut f) => {
            let mut contents = String::new();
            match f.read_to_string(&mut contents) {
                Ok(_) => println!("{}", contents),
                Err(e) => println!("{}", e),
            }
        }
        Err(e) => match e.kind() {
            std::io::ErrorKind::NotFound => match File::create(file_name) {
                Ok(_) => (),
                Err(_) => panic!("failed to create file!"),
            },
            _ => panic!("error: {}", e),
        },
    }

    // second way
    let mut file = File::open(file_name).unwrap_or_else(|error| {
        if error.kind() == std::io::ErrorKind::NotFound {
            File::create(file_name)
                .unwrap_or_else(|error| panic!("Failed to create a file {}", error))
        } else {
            panic!("Failed to open the file {}", error);
        }
    });

    let mut contents = String::new();
    file.read_to_string(&mut contents).unwrap_or_else(|error| {
        panic!("Failed to open read {}", error);
    });

    println!("{}", contents);

    // unwrap & expect
    let _file = File::open(file_name).unwrap(); // returns value of `Ok` or panics if `Err` has
                                                // occured
    let _file = File::open("hello.txt").expect("hello.txt must be in the current directory.");
    // lets you define the error message when panicked.
}
