use std::io;

// using structs defined in lib.rs
use rustdocs::User;

fn main() {
    let user = User::new("Mithra".to_string(), 20);
    println!("{:?}", user);
}
