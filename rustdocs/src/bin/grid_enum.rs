struct Grid {
    x: i32,
    y: i32,
}

enum Direction {
    North,
    South,
    West,
    East,
}

impl Grid {
    fn new(x: i32, y: i32) -> Self {
        Grid { x, y }
    }

    fn move_to(self: &mut Self, d: Direction) {
        match d {
            Direction::North => self.y += 1,
            Direction::South => self.y -= 1,
            Direction::West => self.x -= 1,
            Direction::East => self.x += 1,
        }
    }

    fn get_position(self: &Self) -> (i32, i32) {
        (self.x, self.y)
    }
}

fn main() {
    let mut grid = Grid::new(0, 0);

    loop {
        println!("1. Up\n2. Down\n3. Left\n4. Right\nQ. Quit\nG. Current Position");
        println!("Input: ");

        let mut buf = String::new();
        std::io::stdin()
            .read_line(&mut buf)
            .expect("Enter any option");

        if buf == "Q".to_string() {
            break;
        }

        if buf == "G".to_string() {
            println!("{:?}", grid.get_position());
            continue;
        }

        let input = buf.trim().parse::<i32>().unwrap();
        match input {
            1 => grid.move_to(Direction::North),
            2 => grid.move_to(Direction::South),
            3 => grid.move_to(Direction::West),
            4 => grid.move_to(Direction::East),
            _ => panic!("Wtf dumbo, read the instructions!"),
        }
    }
}
