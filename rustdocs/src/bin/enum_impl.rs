// struct WriteMessage(String);
// struct MoveMessage {
//     x: i32,
//     y: i32,
// }
// struct ChangeColorMessage(i32, i32, i32);
// struct QuitMessage;

// shorthand for above defined structs
#[derive(Debug)]
enum Message {
    Write(String),
    Move { x: i32, y: i32 },
    ChangeColor(i32, i32, i32),
    Quit,
}

impl Message {
    fn call(self: &Self) -> Result<String, String> {
        match self {
            Message::Write(s) => println!("message body: {}", s),
            Message::Move { x, y } => println!("moving to x: {}, and y: {}", x, y),
            Message::ChangeColor(r, g, b) => println!("changing color to ({}, {}, {})", r, g, b),
            Message::Quit => println!("quitting!"),
        }

        Ok("Successfully called".to_string())
    }
}

fn main() {
    let w = Message::Write("Hello, Mithra".to_string());
    let m = Message::Move { x: 3, y: 2 };
    let c = Message::ChangeColor(255, 255, 255);
    let q = Message::Quit;

    match w.call() {
        Ok(s) => println!("{}", s),
        Err(e) => println!("{}", e),
    }
    match m.call() {
        Ok(s) => println!("{}", s),
        Err(e) => println!("{}", e),
    }
    match c.call() {
        Ok(s) => println!("{}", s),
        Err(e) => println!("{}", e),
    }
    match q.call() {
        Ok(s) => println!("{}", s),
        Err(e) => println!("{}", e),
    }
}
