use rand::Rng;
use std::cmp::Ordering;
use std::io;

fn main() {
    // generate random number in-between 1 to 100 (inclusive)
    let some_number = rand::thread_rng().gen_range(1..=100);

    loop {
        let mut number_string: String = String::new();
        io::stdin().read_line(&mut number_string).expect("number.");

        let number: i32 = match number_string.trim().parse() {
            Ok(n) => n,
            Err(_) => {
                println!("Was expecting a number!");
                break;
            }
        };

        match number.cmp(&some_number) {
            Ordering::Less => println!("Try higher!"),
            Ordering::Greater => println!("Try lower!"),
            Ordering::Equal => {
                println!("You won!");
                break;
            }
        }
    }
}
