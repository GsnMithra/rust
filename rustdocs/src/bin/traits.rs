#[path = "../summary.rs"]
mod summary;
use std::cmp::Ordering;

use summary::{Summary, Tweet};

impl<T: Summary> Summary for Vec<T> {
    fn summarize(&self) -> String {
        let mut summary = String::new();
        for v in self {
            summary.push_str(&v.summarize());
            summary.push_str("\n");
        }
        summary[0..summary.len() - 1].to_string()
    }
}

#[derive(Eq, PartialEq, Debug)]
struct User {
    age: u32,
    username: String,
    email: String,
}

impl std::cmp::Ord for User {
    fn cmp(&self, other: &Self) -> Ordering {
        self.age.cmp(&other.age)
    }
}

impl std::cmp::PartialOrd for User {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.age.cmp(&other.age))
    }
}

fn largest<T: std::cmp::PartialOrd>(list: &[T]) -> Option<&T> {
    if list.is_empty() {
        return None;
    }

    let mut max = &list[0];
    for i in 1..list.len() {
        if &list[i] > max {
            max = &list[i];
        }
    }

    Some(max)
}

fn main() {
    let tweet = Tweet {
        username: "Gsn Mithra".to_string(),
        content: "Hello, this is my first news article.".to_string(),
    };

    let tweets = vec![
        Tweet {
            username: "Gsn".to_string(),
            content: "Hellowww!".to_string(),
        },
        Tweet {
            username: "Mithra".to_string(),
            content: "How is it going?".to_string(),
        },
    ];

    let mut users: Vec<User> = vec![
        User {
            username: "Mithra".to_string(),
            email: "mithragsn9@gmail.com".to_string(),
            age: 21,
        },
        User {
            username: "Gsn".to_string(),
            email: "mithragsn1@gmail.com".to_string(),
            age: 20,
        },
    ];

    println!("{:?}", largest(&users).expect("list cannot be empty!"));

    users.sort_by(|a, b| a.username.cmp(&b.username));

    println!("{}", tweet.summarize());
    println!("{}", tweets.summarize());
    println!("{:?}", users);
}
