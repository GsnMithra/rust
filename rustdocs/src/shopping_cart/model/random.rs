use rand::Rng;

pub fn rand_int() -> u32 {
    rand::thread_rng().gen_range(0..10000)
}
