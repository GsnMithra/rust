#[allow(dead_code)]
mod random;

#[derive(Debug, Clone)]
pub struct Item {
    name: String,
    price: u32,
}

impl Item {
    pub fn new(name: String, price: Option<u32>) -> Self {
        match price {
            Some(price) => Self { name, price },
            None => Self {
                name,
                price: random::rand_int(),
            },
        }
    }
}
