pub mod model;
use model::Item;

pub struct ShoppingCart {
    items: Vec<Item>,
}

impl ShoppingCart {
    pub fn new() -> Self {
        Self { items: Vec::new() }
    }

    pub fn add_item(self: &mut Self, item: Item) {
        self.items.push(item)
    }

    pub fn get_cart(self: &Self) -> Vec<Item> {
        self.items.clone()
    }

    pub fn delete_item(self: &mut Self, idx: i32) {
        if idx < 0 && idx as usize >= self.items.len() {
            return;
        }

        self.items.remove(idx as usize);
    }
}
