use rand::Rng;

pub struct Randomize;
impl Randomize {
    pub fn get(limit: i32) -> i32 {
        rand::thread_rng().gen_range(1..=limit)
    }
}

pub fn get() -> i32 {
    rand::thread_rng().gen_range(1..=10)
}
