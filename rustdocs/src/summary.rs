pub struct Tweet {
    pub username: String,
    pub content: String,
}

pub struct NewsArticle {
    pub author: String,
    pub content: String,
    pub date: String,
}

pub trait Summary {
    fn summarize(&self) -> String;
}

impl Summary for Tweet {
    fn summarize(&self) -> String {
        format!("By: {}, Tweet: {}", self.username, self.content)
    }
}

impl Summary for NewsArticle {
    fn summarize(&self) -> String {
        format!(
            "By: {}, On: {}, News: {}",
            self.author, self.date, self.content
        )
    }
}
