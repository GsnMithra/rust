#[allow(dead_code)]
// we cannot have a main function in lib.rs
// mention pub to define the struct public to use
#[derive(Debug)]
pub struct User {
    name: String,
    age: u8,
}

// invalid to mention pub for impl
// but need to mention for the method to be accessed publicly
impl User {
    pub fn new(name: String, age: u8) -> Self {
        Self { name, age }
    }
}
