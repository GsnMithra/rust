use rand::Rng;
use std::ops::Range;

pub fn rand_int(rng: Range) -> i32 {
    rand::thread_rng().gen_range(rng.start..rng.end)
}
