fn get_first_name() -> String {
    "Gsn".to_string()
}

fn get_last_name() -> String {
    "Mithra".to_string()
}

fn main() {
    let first_name = get_first_name();
    let last_name = get_last_name();

    let full_name = first_name + " " + &last_name;
    println!("{full_name}");
}
