fn main() {
    let number: i32 = 10010;

    match number {
        10 => println!("Ten"),
        20 => println!("Twenty"),
        30 => println!("Thirty"),
        _ => println!("Other"),
    }
}
