#[derive(Debug, Clone)]
struct ListNode {
    val: i32,
    next: Option<Box<ListNode>>,
}

impl ListNode {
    fn new(val: i32) -> Self {
        ListNode { val, next: None }
    }
}

#[derive(Debug)]
struct List {
    head: Option<Box<ListNode>>,
    tail: Option<Box<ListNode>>,
}

impl List {
    fn new() -> Self {
        List {
            head: None,
            tail: None,
        }
    }

    fn show(&self) {
        let mut curr = &self.head;

        while let Some(node) = curr {
            print!("{} ", node.val);
            curr = &node.next;
        }
    }

    fn add(&mut self, val: i32) {
        let new_node = Some(Box::new(ListNode::new(val)));
        if self.head.is_none() && self.tail.is_none() {
            self.head = new_node.clone();
            self.tail = self.head.clone();
        } else {
            self.tail.as_mut().unwrap().next = new_node;
            self.tail = self.tail.as_mut().unwrap().next.clone();
        }
    }
}

fn main() {
    let mut list = List::new();

    list.add(10);
    list.add(20);
    list.add(30);
    list.add(40);

    list.show();
}
