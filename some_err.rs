fn divide_numbers(x: i32, y: i32) -> Result<Option<i32>, String> {
    if y == 0 {
        Err("Zero division error!".to_string())
    } else {
        let result = x + y;
        if result >= 10 {
            Ok(Some(result))
        } else {
            Ok(None)
        }
    }
}

fn main() {
    let x = 5;
    let y = 0;

    let result = divide_numbers(x, y);
    let value = match result {
        Ok(n) => n,
        Err(_) => None,
    };

    match value {
        Some(n) => println!("{}", n),
        None => println!("None"),
    }
}
