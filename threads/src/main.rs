use std::sync::{Arc, Mutex};
use std::thread;

// fn main() {
// let string = Arc::new(Mutex::new(String::from("Hello")));

// let mover = Arc::clone(&string);
// let first = thread::spawn(move || {
//     let mut x = mover.lock().unwrap();
//     x.push_str(", Mithra");
// });

// first.join().unwrap();

// println!("{}", string.lock().unwrap());
// }

fn main() {
    let mutex = Arc::new(Mutex::new(0));

    let producer_mutex = Arc::clone(&mutex);
    let producer = thread::spawn(move || {
        for i in 1..=10 {
            let mut value = producer_mutex.lock().unwrap();
            *value += i;
            println!("Producer: Added {}, Total: {}", i, *value);
            drop(value); // Explicitly release the lock
            thread::sleep(std::time::Duration::from_millis(100));
        }
    });

    let consumer_mutex = Arc::clone(&mutex);
    let consumer = thread::spawn(move || {
        for _ in 1..=10 {
            let value = consumer_mutex.lock().unwrap();
            println!("Consumer: Total: {}", *value);
            thread::sleep(std::time::Duration::from_millis(200));
        }
    });

    producer.join().unwrap();
    consumer.join().unwrap();
}
