#[derive(Clone)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

fn check_direction(dir: Direction) {
    match dir {
        Direction::Up => println!("Up"),
        Direction::Down => println!("Down"),
        Direction::Left => println!("Left"),
        Direction::Right => println!("Right"),
    }
}

fn main() {
    let dir: Direction = Direction::Up;

    check_direction(dir.clone());
    check_direction(dir);
}
