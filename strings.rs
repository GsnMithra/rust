fn main() {
    // string slice
    let a: &str = "Hello, Mithra";

    // string
    let b: String = String::from(r#"Mithra is working on "Rust""#);
    println!("{}\n{}", a, b);
}
