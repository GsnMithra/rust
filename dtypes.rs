#[allow(unused)]

fn main() {
    let num = 10;
    let num_static: i8 = 127;
    let name: String = String::from("Mithra");
    let also_name: &str = "Mithra";
    let mut mut_name: String = String::new();
    mut_name.push_str("Gsn");

    println!("{}, {}", num, num_static);
    println!("{}", mut_name);
}
