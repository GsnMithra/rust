use rand::Rng;

pub fn random_level(norm: f64, max_level: usize) -> usize {
    let mut rng = rand::thread_rng();
    let random: f64 = rng.gen_range(0.0..1.0);
    let level = (-random.ln() * norm).floor() as usize;
    level.min(max_level)
}

fn main() {
    for _ in 0..10 {
        println!("{}", random_level(0.5, 16));
    }
}
