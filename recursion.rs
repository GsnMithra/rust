use std::collections::HashMap;
use std::io;

fn fibonacci(dp: &mut HashMap<u128, u128>, n: u128) -> u128 {
    match n {
        0 | 1 => n,
        c => match dp.get(&c) {
            Some(&v) => v,
            None => {
                let result = fibonacci(dp, c - 1) + fibonacci(dp, c - 2);
                dp.insert(c, result);
                result
            }
        },
    }
}

fn main() {
    let mut dp: HashMap<u128, u128> = HashMap::new();
    let mut num: String = "".to_string();

    io::stdin().read_line(&mut num).expect("Error!");
    let number = num.trim().parse::<u128>().unwrap();

    for num in 0..=number {
        print!("{} ", fibonacci(&mut dp, num));
    }

    println!("");
}
