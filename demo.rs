use std::rc::Rc;

struct Node <T> {
    val: T,
    next: Option <Rc <Node <T>>>
}

impl <T> Node <T> {
    fn new (val: T) -> Self {
        Node { val, next: None }
    } 
}

struct LinkedList <T> {
    head: Option <Rc <Node <T>>>,
    tail: Option <Rc <Node <T>>>
}

impl <T> LinkedList <T> {
    fn new () -> Self {
        LinkedList {
            head: None,
            tail: None
        }
    }

    fn add (&mut self, val: T) {
        let new_node: Node <T> = Node::new (val);
        if self.head.is_none () && self.tail.is_none () {
            self.head = Some (Rc::new (new_node));
            self.tail = self.head;
        } else {
            self.tail.unwrap ().next = Some (new_node.into ());
            self.tail = self.tail.unwrap ().next;
        }
    }
}

fn main () {
    let mut list: LinkedList <i32> = LinkedList::new ();
    list.add (10);
    list.add (10);
    list.add (10);
    list.add (10);
    list.add (10);
    list.add (10);
}

