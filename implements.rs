trait GreetPerson {
    fn greet(&self) -> String;
}

impl GreetPerson for &str {
    fn greet(&self) -> String {
        format!("Hello, {}", *self)
    }
}

fn main() {
    let name = "Mithra";

    println!("{}", name.greet());
}
